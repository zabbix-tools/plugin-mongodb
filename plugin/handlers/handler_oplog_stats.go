/*
** Zabbix
** Copyright 2001-2023 Zabbix SIA
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
**/

package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"git.zabbix.com/ap/plugin-support/zbxerr"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type oplogStats struct {
	TimeDiff int `json:"timediff"` // in seconds
}

const (
	oplogReplicaSet  = "oplog.rs"    // the capped collection that holds the oplog for Replica Set Members
	oplogMasterSlave = "oplog.$main" // oplog for the master-slave configuration
)

var oplogQuery = bson.M{"ts": bson.M{"$exists": true}}

// OplogStatsHandler
// https://docs.mongodb.com/manual/reference/method/db.getReplicationInfo/index.html
func OplogStatsHandler(s Session, _ map[string]string) (interface{}, error) {
	var (
		err             error
		firstTs, lastTs int
	)

	localDb := s.DB("local")
	findOptions := options.FindOne()
	findOptions.SetMaxTime(time.Duration(s.GetMaxTimeMS()) * time.Millisecond)

	for _, collection := range []string{oplogReplicaSet, oplogMasterSlave} {
		firstTs, lastTs, err = getTS(collection, localDb, findOptions)
		if err != nil {
			if errors.As(err, &errNotFound) {
				continue
			}

			return nil, err
		}

		break
	}

	jsonRes, err := json.Marshal(oplogStats{firstTs - lastTs})
	if err != nil {
		return nil, zbxerr.ErrorCannotMarshalJSON.Wrap(err)
	}

	return string(jsonRes), nil
}

func getTS(collection string, localDb Database, findOptions *options.FindOneOptions) (int, int, error) {
	findOptions.SetSort(bson.D{{Key: sortNatural, Value: -1}})
	firstTs, err := getOplogStats(localDb, collection, findOptions)
	if err != nil {
		return 0, 0, err
	}

	findOptions.SetSort(bson.D{{Key: sortNatural, Value: 1}})
	lastTs, err := getOplogStats(localDb, collection, findOptions)
	if err != nil {
		return 0, 0, err
	}

	return firstTs, lastTs, nil
}

func getOplogStats(db Database, collection string, opt *options.FindOneOptions) (out int, err error) {
	var result primitive.D

	err = db.C(collection).FindOne(oplogQuery, opt).GetSingle(&result)
	if err != nil {
		if errors.As(err, &errNotFound) {
			return
		}

		return out, zbxerr.ErrorCannotFetchData.Wrap(fmt.Errorf("failed to decode result: %w", err))
	}

	for _, op := range result {
		if op.Key == timestampBSONName {
			if pt, ok := op.Value.(primitive.Timestamp); ok {
				out = int(time.Unix(int64(pt.T), 0).Unix())
			}
		}
	}

	return
}
