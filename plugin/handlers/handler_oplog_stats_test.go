/*
** Zabbix
** Copyright 2001-2023 Zabbix SIA
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
**/

package handlers

import (
	"errors"
	"reflect"
	"testing"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var counter int

func Test_oplogStatsHandler(t *testing.T) {
	var (
		opFirst = &bson.D{{Key: "ts", Value: &primitive.Timestamp{T: uint32(6644097), I: 1}}}
		opLast  = &bson.D{{Key: "ts", Value: &primitive.Timestamp{T: uint32(2178177), I: 1}}}
	)

	mockSession := NewMockConn()
	localDb := mockSession.DB("local")

	dataFunc := func() ([]byte, error) {
		if counter%2 == 0 {
			counter++

			return bson.Marshal(opFirst)
		} else {
			counter++

			return bson.Marshal(opLast)
		}
	}

	type args struct {
		s           Session
		collections []string
	}

	tests := []struct {
		name    string
		args    args
		want    interface{}
		wantErr error
	}{
		{
			name: "Must return 0 if neither oplog.rs nor oplog.$main collection found",
			args: args{
				s:           mockSession,
				collections: []string{},
			},
			want:    "{\"timediff\":0}",
			wantErr: nil,
		},
		{
			name: "Must calculate timediff from oplog.$main collection",
			args: args{
				s:           mockSession,
				collections: []string{oplogMasterSlave},
			},
			want:    "{\"timediff\":4465920}",
			wantErr: nil,
		},
		{
			name: "Must calculate timediff from oplog.rs collection",
			args: args{
				s:           mockSession,
				collections: []string{oplogReplicaSet},
			},
			want:    "{\"timediff\":4465920}",
			wantErr: nil,
		},
	}

	for _, tt := range tests {
		for _, col := range tt.args.collections {
			q, _ := localDb.C(col).Find(oplogQuery)
			q.(*MockMongoQuery).DataFunc = dataFunc
		}

		t.Run(tt.name, func(t *testing.T) {
			got, err := OplogStatsHandler(tt.args.s, nil)
			if !errors.Is(err, tt.wantErr) {
				t.Errorf("oplogStatsHandler() error = %v, wantErr %v", err, tt.wantErr)

				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("oplogStatsHandler() got = %v, want %v", got, tt.want)
			}
		})
	}
}
