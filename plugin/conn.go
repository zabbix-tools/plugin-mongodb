/*
** Zabbix
** Copyright 2001-2023 Zabbix SIA
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
**/

package plugin

import (
	"context"
	"crypto/tls"
	"sync"
	"time"

	"git.zabbix.com/ap/mongodb/plugin/handlers"
	"git.zabbix.com/ap/plugin-support/tlsconfig"
	"git.zabbix.com/ap/plugin-support/uri"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type MongoConn struct {
	addr           string
	timeout        time.Duration
	lastTimeAccess time.Time
	session        mongo.Session
}

// DB shadows *mgo.DB to returns a Database interface instead of *mgo.Database.
func (conn *MongoConn) DB(name string) handlers.Database {
	conn.checkConnection()

	return &MongoDatabase{Database: conn.session.Client().Database(name)}
}

func (conn *MongoConn) DatabaseNames() (names []string, err error) {
	conn.checkConnection()

	return conn.session.Client().ListDatabaseNames(context.Background(), bson.D{})
}

func (conn *MongoConn) Ping() error {
	Impl.Debugf("executing ping for address: %s", conn.addr)
	result := conn.session.Client().Database("admin").RunCommand(
		context.Background(),
		&bson.D{{Key: "ping", Value: 1}, {Key: "maxTimeMS", Value: conn.GetMaxTimeMS()}},
	)

	err := result.Err()
	if err != nil {
		Impl.Debugf("failed to ping database %s, %s", conn.addr, err.Error())

		return err
	}

	Impl.Debugf("ping successful for address: %s", conn.addr)

	return nil
}

func (conn *MongoConn) GetMaxTimeMS() int64 {
	return conn.timeout.Milliseconds()
}

// updateAccessTime updates the last time a connection was accessed.
func (conn *MongoConn) updateAccessTime() {
	conn.lastTimeAccess = time.Now()
}

// checkConnection implements db reconnection.
func (conn *MongoConn) checkConnection() {
	if err := conn.Ping(); err != nil {
		//TODO: implement reconnect
		// conn.session.Refresh()
		Impl.Debugf("Attempt to reconnect: %s", conn.addr)
	}
}

// MongoDatabase wraps a mgo.Database to embed methods in models.
type MongoDatabase struct {
	// *mgo.Database
	*mongo.Database
}

// C shadows *mongo.DB to returns a Database interface instead of *mgo.Database.
func (d *MongoDatabase) C(name string) handlers.Collection {
	return &MongoCollection{Collection: d.Collection(name)}
}

func (d *MongoDatabase) CollectionNames() (names []string, err error) {
	return d.Database.ListCollectionNames(context.Background(), bson.D{})
}

// Run shadows *mgo.DB to returns a Database interface instead of *mgo.Database.
func (d *MongoDatabase) Run(cmd, result interface{}) error {
	return d.Database.RunCommand(context.Background(), cmd).Decode(result)
}

// MongoCollection wraps a mongo.Collection to embed methods in models.
type MongoCollection struct {
	*mongo.Collection
}

// Collection is an interface to access to the collection struct.

// Find shadows *mgo.Collection to returns a Query interface instead of *mgo.Query.
func (c *MongoCollection) Find(query interface{}, opts ...*options.FindOptions) (handlers.Query, error) {
	cursor, err := c.Collection.Find(context.Background(), query, opts...)
	if err != nil {
		return nil, err
	}

	return &MongoQuery{Cursor: cursor}, nil
}

// FindOne shadows *mgo.Collection to returns a Query interface instead of *mgo.Query.
func (c *MongoCollection) FindOne(query interface{}, opts ...*options.FindOneOptions) handlers.Query {
	return &MongoQuery{SingleResult: c.Collection.FindOne(context.Background(), query, opts...)}
}

// Query is an interface to access to the query struct

// MongoQuery wraps a mgo.Query to embed methods in models.
type MongoQuery struct {
	*mongo.Cursor
	*mongo.SingleResult
}

func (q *MongoQuery) Count() (int, error) {
	var in []interface{}
	err := q.Cursor.All(context.Background(), &in)
	if err != nil {
		return 0, err
	}

	return len(in), nil
}

func (q *MongoQuery) Get(result interface{}) error {
	if q.Cursor.Err() != nil {
		return q.Cursor.Err()
	}

	return q.Cursor.All(context.Background(), result)
}

func (q *MongoQuery) GetSingle(result interface{}) error {
	if q.SingleResult.Err() != nil {
		return q.SingleResult.Err()
	}

	return q.SingleResult.Decode(result)
}

// ConnManager is thread-safe structure for manage connections.
type ConnManager struct {
	sync.Mutex
	connMutex   sync.Mutex
	connections map[uri.URI]*MongoConn
	keepAlive   time.Duration
	timeout     time.Duration
	Destroy     context.CancelFunc
}

// NewConnManager initializes connManager structure and runs Go Routine that watches for unused connections.
func NewConnManager(keepAlive, timeout, hkInterval time.Duration) *ConnManager {
	ctx, cancel := context.WithCancel(context.Background())

	connMgr := &ConnManager{
		connections: make(map[uri.URI]*MongoConn),
		keepAlive:   keepAlive,
		timeout:     timeout,
		Destroy:     cancel, // Destroy stops originated goroutines and close connections.
	}

	go connMgr.housekeeper(ctx, hkInterval)

	return connMgr
}

// closeUnused closes each connection that has not been accessed at least within the keepalive interval.
func (c *ConnManager) closeUnused() {
	c.connMutex.Lock()
	defer c.connMutex.Unlock()

	for uri, conn := range c.connections {
		if time.Since(conn.lastTimeAccess) > c.keepAlive {
			conn.session.EndSession(context.Background())
			_ = conn.session.Client().Disconnect(context.Background())
			delete(c.connections, uri)
			Impl.Debugf("Closed unused connection: %s", uri.Addr())
		}
	}
}

// closeAll closes all existed connections.
func (c *ConnManager) closeAll() {
	c.connMutex.Lock()
	for uri, conn := range c.connections {
		conn.session.EndSession(context.Background())
		_ = conn.session.Client().Disconnect(context.Background())
		delete(c.connections, uri)
	}
	c.connMutex.Unlock()
	Impl.Debugf("Closed all connections")
}

// housekeeper repeatedly checks for unused connections and close them.
func (c *ConnManager) housekeeper(ctx context.Context, interval time.Duration) {
	ticker := time.NewTicker(interval)

	for {
		select {
		case <-ctx.Done():
			ticker.Stop()
			c.closeAll()

			return
		case <-ticker.C:
			c.closeUnused()
		}
	}
}

// create creates a new connection with given credentials.
func (c *ConnManager) create(uri uri.URI, details tlsconfig.Details) (*MongoConn, error) {
	opt, err := c.createOptions(uri, details)
	if err != nil {
		return nil, err
	}

	client, err := mongo.NewClient(opt)
	if err != nil {
		return nil, err
	}

	err = client.Connect(context.Background())
	if err != nil {
		return nil, err
	}

	session, err := client.StartSession()
	if err != nil {
		return nil, err
	}

	err = session.Client().Ping(context.Background(), readpref.Nearest())
	if err != nil {
		return nil, err
	}

	c.connMutex.Lock()
	defer c.connMutex.Unlock()

	c.connections[uri] = &MongoConn{
		addr:           uri.Addr(),
		timeout:        c.timeout,
		lastTimeAccess: time.Now(),
		session:        session,
	}

	Impl.Debugf("Created new connection: %s", uri.Addr())

	return c.connections[uri], nil
}

// get returns a connection with given uri if it exists and also updates lastTimeAccess, otherwise returns nil.
func (c *ConnManager) createOptions(uri uri.URI, details tlsconfig.Details) (*options.ClientOptions, error) {
	opt := options.Client()
	if uri.User() != "" {
		creds := options.Credential{}
		creds.Username = uri.User()
		creds.Password = uri.Password()
		creds.PasswordSet = true
		opt = opt.SetAuth(creds)
	}

	if details.TlsConnect != "" {
		err := setTLSConfig(opt, details)
		if err != nil {
			return nil, err
		}
	}

	opt.SetHosts([]string{uri.Addr()})
	opt.SetDirect(true)
	opt.SetConnectTimeout(c.timeout)
	opt.SetServerSelectionTimeout(c.timeout)
	opt.SetMaxPoolSize(1)

	return opt, nil
}

func setTLSConfig(opt *options.ClientOptions, details tlsconfig.Details) (err error) {
	var cfg *tls.Config

	switch details.TlsConnect {
	case "required":
		cfg = &tls.Config{InsecureSkipVerify: true}
	case "verify_ca":
		cfg, err = tlsconfig.CreateConfig(details, true)
		if err != nil {
			return
		}
	case "verify_full":
		cfg, err = tlsconfig.CreateConfig(details, false)
		if err != nil {
			return
		}
	}

	opt.SetTLSConfig(cfg)

	return nil
}

// get returns a connection with given uri if it exists and also updates lastTimeAccess, otherwise returns nil.
func (c *ConnManager) get(uri uri.URI) *MongoConn {
	c.connMutex.Lock()
	defer c.connMutex.Unlock()

	if conn, ok := c.connections[uri]; ok {
		conn.updateAccessTime()

		return conn
	}

	return nil
}

// GetConnection returns an existing connection or creates a new one.
func (c *ConnManager) GetConnection(uri uri.URI, details tlsconfig.Details) (conn *MongoConn, err error) {
	c.Lock()
	defer c.Unlock()

	conn = c.get(uri)

	if conn == nil {
		conn, err = c.create(uri, details)
	}

	return
}
